# Weekday API Deploy

This is a simple Ansible playbook for deploying the [weekday-api](https://gitlab.com/cypher_zero/weekday-api) project to an EC2 instance.
